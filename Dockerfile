FROM alpine:edge

RUN apk add --update --no-cache \
		--repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ \
		bash libgcc libstdc++ openssl ca-certificates \
		git curl wget bzip2 tar make gcc clang g++ \
		python linux-headers paxctl binutils-gold \
		autoconf bison zlib-dev openssl-dev \
		nodejs nodejs-npm docker yarn \
		vips-dev fftw-dev \
		libjpeg-turbo-dev jpeg-dev

RUN curl -sL https://github.com/github/git-lfs/releases/download/v2.0.1/git-lfs-linux-amd64-2.0.1.tar.gz | tar xvzf - -C . && mv git-lfs-2.0.1/git-lfs /usr/bin/ && rm -rf git-lfs-2.0.1 && git lfs install

# glibc for alpine
RUN wget -O - "http://github.com/cusspvz/node.docker/raw/master/apks.tar.gz" | tar xvz -C /tmp && \
    apk add --force --allow-untrusted \
        /tmp/apks/glibc-2.21-r2.apk \
        /tmp/apks/glibc-bin-2.21-r2.apk \
    && \
    /usr/glibc/usr/bin/ldconfig /lib /usr/glibc/usr/lib && \
    rm -fR /tmp/apks /var/cache/apk/*


# Docker compose
RUN wget -O /usr/bin/docker-compose https://github.com/docker/compose/releases/download/1.18.0/docker-compose-Linux-x86_64 && \
    chmod +x /usr/bin/docker-compose && \
    /usr/bin/docker-compose --version

# Note: Latest version of kubectl may be found at:
# https://aur.archlinux.org/packages/kubectl-bin/
ENV KUBE_LATEST_VERSION="v1.14.1"
# Note: Latest version of helm may be found at:
# https://github.com/kubernetes/helm/releases
ENV HELM_VERSION="v2.9.1"

RUN apk add --no-cache ca-certificates bash \
    && wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && wget -q http://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm

# TODO: REMOVE THIS once alpine has this changed
# PATCH for the SEGFAULT caused by the different Thread stack size on MUSL
# ADD https://raw.githubusercontent.com/jubel-han/dockerfiles/master/common/stack-fix.c /lib/stack-fix.c
#RUN set -ex \
#    && apk add --no-cache  --virtual .build-deps build-base \
#    && gcc  -shared -fPIC /lib/stack-fix.c -o /lib/stack-fix.so
#ENV LD_PRELOAD /lib/stack-fix.so

# Install heroku cli
RUN yarn global add heroku-cli

WORKDIR /app
ADD https://raw.githubusercontent.com/cusspvz/node.docker/master/src/entrypoint /bin/entrypoint
RUN chmod +x /bin/entrypoint
ENTRYPOINT [ "/bin/entrypoint" ]
CMD [ "" ]

